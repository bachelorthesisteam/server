# Server

A lightweight node.js server providing a REST interface for the localisation
sample database used for my bachelor thesis

## Configuration

The config file has to hold the following properties for the server to run:

```
module.exports = {
  httpPort: 8080,
  httpsPort: 8081,
  secret: 'fahlcxbkewhpcjvbmilv',
  database: 'mongodb://localhost:27017/robotino_database',
  httpsKeyPath: 'certificates/server.key',
  httpsCertPath: 'certificates/server.crt',
  rootUser: {
    username: 'admin',
    password: 'admin',
    firstname: 'matthias',
    lastname: 'lehner',
    email: 'admin@mail.de',
    admin: true
  }
};
```

## Installation

``` bash
  $ npm install
```

To install all dependencies listed in package.json

## Usage

``` bash
  $ npm start
```

This launches forever.js which launches the actual script to restart the server
automatically if it crashes
