'use strict';

const HTTP_FALLBACK_PORT = 8080;
const HTTPS_FALLBACK_PORT = 8081;

var express = require('express');
var app = express();

var bodyParser = require('body-parser'); // json parsing
var jsonParser = bodyParser.json();

var assert = require('assert'); // assertion
var morgan = require('morgan'); // logging
var mongoose = require('mongoose'); // mongo db access
var jwt = require('jsonwebtoken'); // token creation

var config = require('./config'); // configuration

var User = require('./models/user'); // User
var Sample = require('./models/sample'); // Sample
var Experiment = require('./models/experiment'); // Experiment
var Platform = require('./models/platform'); // Platform

var fs = require('fs');
var https = require('https');

var Grid = require('gridfs-stream');
var ObjectID = require('mongodb').ObjectID;

var BusboyBodyParser = require('busboy-body-parser');

var httpPort = config.httpPort || HTTP_FALLBACK_PORT;
var httpsPort = config.httpsPort || HTTPS_FALLBACK_PORT;

mongoose.Promise = global.Promise;

mongoose.connect(config.database);
var connection = mongoose.connection;
var gfs = Grid(connection.db, mongoose.mongo);

app.use(BusboyBodyParser({
  limit: '15mb'
}));

app.use(bodyParser.urlencoded({
  extended: false
}));

app.use(bodyParser.json());

app.use(morgan('dev'));

var options = { // https key and cert
  key: fs.readFileSync('./certificates/server.key'),
  cert: fs.readFileSync('./certificates/server.crt')
};

var server = https.createServer(options, app);

// -------------------------------------------------------------------------- //
// route middleware to authenticate and check token                           //
// -------------------------------------------------------------------------- //

var apiRoutes = express.Router();
var adminRoutes = express.Router();

// api routes
apiRoutes.use((request, response, next) => {
  // check header for auth token
  var token = request.headers['authorization'];
  // decode token
  if (token) {
    // verifies secret
    jwt.verify(token, config.secret, (error, decoded) => {
      if (error) {
        return response.status(500).send({
          error: error
        });
      } else {
        // if everything is good, save to request for use in other routes
        request.decoded = decoded;
        next();
      }
    });
  } else {
    // if there is no token return an error
    return response.status(403).send({
      error: {
        title: 'authentication failed',
        description: 'no authentication token provided'
      }
    });
  }
});

// admin routes (only user management)
adminRoutes.use((request, response, next) => {
  // check header for auth token
  var token = request.headers['authorization'];
  // decode token
  if (token) {
    // verifies secret and checks exp
    jwt.verify(token, config.secret, (error, decoded) => {
      if (error) {
        return response.status(500).send({
          error: error
        });
      } else {
        // check whether the user has the correct permissions
        if (decoded._doc.admin == true) {
          request.decoded = decoded;
          next();
        } else {
          return response.status(500).send({
            error: {
              title: 'authentication failed',
              description: 'user has no admin privilege'
            }
          });
        }
      }
    });
  } else {
    // if there is no token return an error
    return response.status(403).send({
      error: {
        title: 'authentication failed',
        description: 'no authentication token provided'
      }
    });
  }
});

// -------------------------------------------------------------------------- //
// authentication                                                             //
// -------------------------------------------------------------------------- //

// endpoint to authenticate
app.post('/v1/authenticate', (request, response) => {
  // find the user
  User.findOne({
    username: request.body.username
  }, (error, user) => {
    if (error)
      throw error;
    if (!user) {
      return response.status(500).send({
        error: error
      });
    } else if (user) {
      // check if password matches
      user.comparePassword(request.body.password, (error, match) => {
        if (match) {
          user.password = undefined;
          var token = jwt.sign(user, config.secret);
          return response.status(200).send({
            user: user,
            token: token
          });
        } else {
          return response.status(500).send({
            error: {
              title: 'authentication failed',
              description: 'the provided password is not correct'
            }
          });
        }
      });
    }
  });
});

// -------------------------------------------------------------------------- //
// user management                                                            //
// -------------------------------------------------------------------------- //

// endpoint to retrieve all users
apiRoutes.get('/v1/users', jsonParser, (request, response) => {
  User.find({}, (error, results) => {
    if (error) {
      return response.status(500).send({
        error: error
      });
    } else {
      results.forEach((user) => { user.password = undefined; });
      return response.status(200).send(results);
    }
  });
});

// endpoint to create a new user
adminRoutes.post('/v1/users', jsonParser, (request, response) => {
  if (request.body) {
    var user = new User(request.body);
    user.save((error) => {
      if (error) {
        return response.status(500).send({
          error: error
        });
      } else {
        user.password = undefined;
        return response.status(201).send(user);
      }
    });
  } else {
    return handleMalformedRequest(response);
  }
});

// endpoint to retrieve a user
apiRoutes.get('/v1/users/:userId', jsonParser, (request, response) => {
  if (request.params) {
    if (request.params.userId) {
      User.findById(request.params.userId, (error, user) => {
        if (error) {
          return response.status(500).send({
            success: false,
            error: error
          });
        } else {
          if (user) {
            user.password = undefined;
          }
          return response.status(200).send(user);
        }
      });
    } else {
      return handleMalformedRequest(response);
    }
  } else {
    return handleMalformedRequest(response);
  }
});

// endpoint to update a user
apiRoutes.put('/v1/users/:userId', jsonParser, (request, response) => {
  if (request.body) {
    // if password has been updated, recreate entire user to create password hash
    if (request.body.user.password && request.body.user.password.length > 0) {
      var user = new User(request.body.user);
      User.findById(request.params.userId).remove((error, result) => {
        if (error) {
          return response.status(500).send({
            error: error
          });
        } else {
          user.save((error) => {
            if (error) {
              return response.status(500).send({
                error: error
              });
            } else {
              return response.status(200).send();
            }
          });
        }
      });
    } else {
      // otherwise just adjust permissions
      User.update({
        _id: request.body.user._id
      }, {
        $set: {
          admin: request.body.user.admin
        }
      }, (error, numerOfAffectedItems) => {
        if (error) {
          return response.status(500).send({
            error: error
          });
        } else {
          return response.status(200).send();
        }
      });
    }
  } else {
    return handleMalformedRequest(response);
  }
});

// endpoint to delete a user
adminRoutes.delete('/v1/users/:userId', jsonParser, (request, response) => {
  if (request.params) {
    if (request.params.userId) {
      User.findById(request.params.userId).remove((error, result) => {
        if (error) {
          return response.status(500).send({
            error: error
          });
        } else {
          return response.status(200).send();
        }
      });
    } else {
      return handleMalformedRequest(response);
    }
  } else {
    return handleMalformedRequest(response);
  }
});

// -------------------------------------------------------------------------- //
// experiment management                                                      //
// -------------------------------------------------------------------------- //

// endpoint to retrieve all experiments
apiRoutes.get('/v1/experiments', jsonParser, (request, response) => {
  Experiment.find({}, (error, results) => {
    if (error) {
      return response.status(500).send({
        error: error
      });
    } else {
      return response.status(200).send(results);
    }
  });
});

// endpoint to create a new experiment
apiRoutes.post('/v1/experiments', jsonParser, (request, response) => {
  if (request.body) {
    var experiment = new Experiment(request.body);
    experiment.save((error) => {
      if (error) {
        return response.status(500).send({
          error: error
        });
      } else {
        return response.status(201).send(experiment);
      }
    });
  } else {
    return handleMalformedRequest(response);
  }
});

// endpoint to retrieve an experiment
apiRoutes.get('/v1/experiments/:experimentId', jsonParser, (request, response) => {
  if (request.params) {
    if (request.params.experimentId) {
      Experiment
        .findById(request.params.experimentId, (error, experiment) => {
        if (error) {
          return response.status(500).send({
            error: error
          });
        } else {
          return response.status(200).send(experiment);
        }
      });
    } else {
      return handleMalformedRequest(response);
    }
  } else {
    return handleMalformedRequest(response);
  }
});

// endpoint to update a experiment
apiRoutes.put('/v1/experiments/:experimentId', jsonParser, (request, response) => {
  if (request.body) {
    var experiment = new Experiment(request.body);
    Experiment
      .findById(request.params.experimentId)
      .remove((error, result) => {
      if (error) {
        return response.status(500).send({
          error: error
        });
      } else {
        experiment.save((error) => {
          if (error) {
            return response.status(500).send({
              error: error
            });
          } else {
            return response.status(200).send();
          }
        });
      }
    });
  } else {
    return handleMalformedRequest(response);
  }
});

// endpoint to delete a experiment with given id
apiRoutes.delete('/v1/experiments/:experimentId', jsonParser, (request, response) => {
  if (request.params) {
    if (request.params.experimentId) {
      Experiment.findById(request.params.experimentId).remove((error, result) => {
        if (error) {
          return response.status(500).send({
            error: error
          });
        } else {
          return response.status(200).send();
        }
      });
    } else {
      return handleMalformedRequest(response);
    }
  } else {
    return handleMalformedRequest(response);
  }
});

// -------------------------------------------------------------------------- //
// platform management                                                        //
// -------------------------------------------------------------------------- //

// endpoint to retrieve all platforms
apiRoutes.get('/v1/platforms', jsonParser, (request, response) => {
  Platform.find({}, (error, results) => {
    if (error) {
      return response.status(500).send({
        error: error
      });
    } else {
      return response.status(200).send(results);
    }
  });
});

// endpoint to create a new platform
apiRoutes.post('/v1/platforms', jsonParser, (request, response) => {
  if (request.body) {
    var platform = new Platform(request.body);
    platform.save((error) => {
      if (error) {
        return response.status(500).send({
          error: error
        });
      } else {
        return response.status(201).send(platform);
      }
    });
  } else {
    return handleMalformedRequest(response);
  }
});

// endpoint to retrieve an platform
apiRoutes.get('/v1/platforms/:platformId', jsonParser, (request, response) => {
  if (request.params) {
    if (request.params.platformId) {
      Platform
        .findById(request.params.platformId, (error, platform) => {
        if (error) {
          return response.status(500).send({
            error: error
          });
        } else {
          return response.status(200).send(platform);
        }
      });
    } else {
      return handleMalformedRequest(response);
    }
  } else {
    return handleMalformedRequest(response);
  }
});

// endpoint to update a platform
apiRoutes.put('/v1/platforms/:platformId', jsonParser, (request, response) => {
  if (request.body) {
    var platform = new Platform(request.body);
    Platform
      .findById(request.params.platformId)
      .remove((error, result) => {
      if (error) {
        return response.status(500).send({
          error: error
        });
      } else {
        platform.save((error) => {
          if (error) {
            return response.status(500).send({
              error: error
            });
          } else {
            return response.status(200).send();
          }
        });
      }
    });
  } else {
    return handleMalformedRequest(response);
  }
});

// endpoint to delete a platform with given id
apiRoutes.delete('/v1/platforms/:platformId', jsonParser, (request, response) => {
  if (request.params) {
    if (request.params.platformId) {
      Platform.findById(request.params.PlatformId).remove((error, result) => {
        if (error) {
          return response.status(500).send({
            error: error
          });
        } else {
          return response.status(200).send();
        }
      });
    } else {
      return handleMalformedRequest(response);
    }
  } else {
    return handleMalformedRequest(response);
  }
});

// -------------------------------------------------------------------------- //
// sample management                                                          //
// -------------------------------------------------------------------------- //

// endpoint to retrieve all samples collected in given experiment
apiRoutes.get('/v1/samples', jsonParser, (request, response) => {
  Sample.find({}, (error, results) => {
    if (error) {
      return response.status(500).send({
        error: error
      });
    } else {
      return response.status(200).send(results);
    }
  });
});

// endpoint to create a new sample
apiRoutes.post('/v1/samples', jsonParser, (request, response) => {
  if (request.body) {
    var sample = new Sample(request.body);
    sample.save((error) => {
      if (error) {
        return response.status(500).send({
          error: error
        });
      } else {
        return response.status(201).send(sample);
      }
    });
  } else {
    return handleMalformedRequest(response);
  }
});

// endpoint to retrieve a sample with given id
apiRoutes.get('/v1/samples/:sampleId', jsonParser, (request, response) => {
  if (request.params) {
    if (request.params.sampleId) {
      Platform
        .findById(request.params.sampleId, (error, sample) => {
        if (error) {
          return response.status(500).send({
            error: error
          });
        } else {
          return response.status(200).send(sample);
        }
      });
    } else {
      return handleMalformedRequest(response);
    }
  } else {
    return handleMalformedRequest(response);
  }
});

// endpoint to update a sample
apiRoutes.put('/v1/samples/:sampleId', jsonParser, (request, response) => {
  if (request.body) {
    var sample = new Sample(request.body);
    Sample
      .findById(request.params.sampleId)
      .remove((error, result) => {
      if (error) {
        return response.status(500).send({
          error: error
        });
      } else {
        sample.save((error) => {
          if (error) {
            return response.status(500).send({
              error: error
            });
          } else {
            return response.status(200).send();
          }
        });
      }
    });
  } else {
    return handleMalformedRequest(response);
  }
});

// endpoint to delete a sample with given id
apiRoutes.delete('/v1/samples/:sampleId', jsonParser, (request, response) => {
  if (request.params) {
    if (request.params.sampleId) {
      Sample.findById(request.params.sampleId).remove((error, result) => {
        if (error) {
          return response.status(500).send({
            error: error
          });
        } else {
          return response.status(200).send();
        }
      });
    } else {
      return handleMalformedRequest(response);
    }
  } else {
    return handleMalformedRequest(response);
  }
});

// -------------------------------------------------------------------------- //
// file management                                                            //
// -------------------------------------------------------------------------- //

// endpoint to retrieve all files
apiRoutes.get('/v1/files', jsonParser, (request, response) => {
  mongoose.connection.collection('fs.files')
    .find({})
    .toArray((error, results) => {
      if (error) {
        return response.status(500).send({
          error: error
        });
      } else {
        return response.status(200).send(results);
      }
    });
});

// endpoint to insert a file into the database
apiRoutes.post('/v1/files', jsonParser, (request, response) => {
  if (request.files && request.files.data) {
    var part = request.files.data;
    var extension = part.name.split(/[. ]+/).pop();
    var _id = new ObjectID();

    var writeStream = gfs.createWriteStream({
      _id: _id,
      filename: part.name,
      mode: 'w',
      content_type: part.mimetype
    });
    writeStream.on('close', () => {
      mongoose.connection.collection('fs.files')
        .find({
          _id: _id
        })
        .toArray((error, results) => {
          if (results.length > 0) {
            return response.status(201).send(results[0]);
          } else {
            return response.status(500).send({
              error: {
                title: 'creating file failed',
                description: 'creating the given file in the database failed'
              }
            });
          }
        });
    });
    writeStream.on('error', () => {
      return response.status(500).send({
        error: {
          title: 'creating file failed',
          description: 'creating the given file in the database failed'
        }
      })
    })
    writeStream.write(part.data);
    writeStream.end();
  } else {
    return handleMalformedRequest(response);
  }
});

// endpoint to retrieve a file from the database
apiRoutes.get('/v1/files/:fileId', jsonParser, (request, response) => {
  if (request.params) {
    if (request.params.fileId) {
      var readstream = gfs.createReadStream({
        _id: request.params.fileId
      });
      //error handling, e.g. file does not exist
      readstream.on('error', (error) => {
        return response.status(500).send({
          error: error
        });
      });
      readstream.pipe(response);
    } else {
      return handleMalformedRequest(response);
    }
  } else {
    return handleMalformedRequest(response);
  }
});

// endpoint to update a file in the database
apiRoutes.put('/v1/files/:fileId', jsonParser, function(request, response) {
  return response.status(501).send({
    error: {
      title: 'not implemented',
      description: 'updating file not implemented.'
    }
  });
});

// endpoint to delete a file from the database
apiRoutes.delete('/v1/files/:fileId', jsonParser, function(request, response) {
  if (request.params) {
    if (request.params.fileId) {
      gfs.remove({
        '_id': request.params.fileId
      }, function(error) {
        if (error) {
          return response.status(500).send({
            error: {
              title: 'deleting file failed',
              description: 'the file could not ne deleted'
            }
          });
        } else {
          return response.status(200).send();
        }
      });
    } else {
      return handleMalformedRequest(response);
    }
  } else {
    return handleMalformedRequest(response);
  }
});

// -------------------------------------------------------------------------- //
// helper methods                                                             //
// -------------------------------------------------------------------------- //

// function that takes over the handling of a malformed request
function handleMalformedRequest(response) {
  return response.status(500).send({
    error: {
      title: 'request failed',
      description: 'the request does not conform to the api standards'
    }
  });
}

// function to create an initial user as specified in the config file
function createInitialUser() {
  if (!config.rootUser) {
    console.log('Root user not set.');
  }
  User.find({
    username: config.rootUser.username
  }, (error, users) => {
    if (error) {
      console.log('Creating root user failed.');
      return;
    }
    if (users && users.length > 0) {
      return;
    }
    var user = new User({
      username: config.rootUser.username,
      password: config.rootUser.password,
      firstname: config.rootUser.firstname,
      lastname: config.rootUser.lastname,
      email: config.rootUser.email,
      admin: config.rootUser.admin
    });
    user.save((error) => {
      if (error) {
        console.log(error);
      } else {
        console.log('root user created.');
      }
    });
  });
}

// -------------------------------------------------------------------------- //
// get an instance of the router for api routes                               //
// -------------------------------------------------------------------------- //

app.use('/api', apiRoutes);
app.use('/admin', adminRoutes);

// -------------------------------------------------------------------------- //
// create initial user                                                        //
// -------------------------------------------------------------------------- //

createInitialUser();

// -------------------------------------------------------------------------- //
// invoke listenting                                                          //
// -------------------------------------------------------------------------- //

server.listen(httpsPort, () => {
  console.log(`app is listening on port ${httpsPort}`);
});

// ---------------------------------------------------------
// HTTP redirecting
// ---------------------------------------------------------

// set up plain http server
var http = express();

// set up a route to redirect http to https
http.get('*', function(request, response) {
  response.redirect('https://localhost' + request.url)
})

// have it listen on 80
http.listen(httpPort);
