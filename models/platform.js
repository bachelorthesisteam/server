var mongoose = require('mongoose');
var extend = require('mongoose-schema-extend');
var Schema = mongoose.Schema;

// model for version information
var Version = new Schema({
  major: { type: Number, required: true },
  minor: { type: Number, required: true },
  revision: { type: Number, required: true }
});

// model for the platform used for sampling
var Platform = new Schema({
  name: { type: String, required: true, unique: true },
  manufacturer: { type: String, required: false },
  version: { type: Version, required: true }
});

module.exports = mongoose.model('Platform', Platform);
