var mongoose = require('mongoose');
var extend = require('mongoose-schema-extend');
var Schema = mongoose.Schema;

// model for an experiment
var Experiment = new Schema({
  name: { type: String, required: true, unique: true },
  author: { type: Schema.ObjectId, required: true },
  description: {type: String, required: false}
});

module.exports = mongoose.model('Experiment', Experiment);
