var mongoose = require('mongoose');
var extend = require('mongoose-schema-extend');
var Schema = mongoose.Schema;

// abstract base model for annotations like Velocity and Configuration
var Annotation = new Schema({
  // domains of the information given by this annotation as a hint for
  // interpretation
  domains: [String]
});

// model of a point in three dimensional space
var Point = new Schema({
  x: { type: Number, required: true },
  y: { type: Number, required: true },
  z: { type: Number, required: true }
});

// model of a three dimensional vector in free space
var Vector = new Schema({
  x: { type: Number, required: true },
  y: { type: Number, required: true },
  z: { type: Number, required: true }
});

// model of the orientation in free space given represented by a quaternion
// according to the ros-standards
var Orientation = new Schema({
  x: { type: Number, required: true },
  y: { type: Number, required: true },
  z: { type: Number, required: true },
  w: { type: Number, required: true }
})

// model for the velocity in free space split up into its linear and angular
// components
var Velocity = Annotation.extend({
  linear: { type: Vector, required: true },
  angular: { type: Vector, required: true }
});

// model for position and orientation in free space combined
var Configuration = Annotation.extend({
  position: { type: Point, required: true },
  orientation: { type: Orientation, required: true }
});

// model for the timespan between two timestamps
var Timespan = new Schema({
  startTimestamp: { type: Number, required: true },
  endTimestamp: { type: Number, required: true }
});

// abstract base model for discrete and continous medi
var Media = new Schema({
  configuration: { type: Configuration, required: true },
  tags: [String],
  data: [Schema.Types.Mixed],
  meta: { type: Schema.Types.Mixed, required: false }
});

// model for representing discrete media like a sampled sensor value
var DiscreteMedia = new Schema({
  timestamp: { type: Number, required: true }
});

// model for representing continous media like video
var ContinuousMedia = new Schema({
  timespan: { type: Timespan, required: true }
});

// model of the central sample persistance structure
var Sample = new Schema({
  experiment: { type: Schema.ObjectId, required: true },
  run: { type: Number, required: true },
  platform: { type: Schema.ObjectId, required: true },
  annotations: [Schema.Types.Mixed],
  media: [Media]
});

module.exports = mongoose.model('Sample', Sample);

// image meta example

/*
meta: {
  orientation: { type: String, required: true },
  aperture: { type: Number, required: false },
  shutterSpeed: { type: Number, required: false }
  focalLength: { type: Number, required: false },
  meteringMode: { type: Number, required: false },
  isoSpeed: { type: Number, required: false }
}
*/

// Das REST interface abstrahiert von der Datenbank -> später tauschbar
// einzelne node/ python script zum upload der datein (author etc werden hier specifiziert)
// batch upload endpoint an den ein tar von allen samples hochgeladen werden kann
// und dann dort verarbeitet werden
// schema für experiment und run
