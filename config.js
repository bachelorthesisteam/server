module.exports = {
  httpPort: 8080,
  httpsPort: 8081,
  secret: 'fahlcxbkewhpcjvbmilv',
  database: 'mongodb://localhost:27017/robotino_database',
  rootUser: {
    username: 'admin',
    password: 'admin',
    firstname: 'matthias',
    lastname: 'lehner',
    email: 'admin@mail.de',
    admin: true
  }
};
